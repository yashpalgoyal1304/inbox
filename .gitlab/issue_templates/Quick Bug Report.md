<!-- only use this template if u have thoroughly gone through and understood default template already -->
<!-- 
    Please be careful when/after writing #  for example in logs, code, or versions of linux
    - use code blocks - single backticks (`) before and after it, like this - `#1618`
    - use code span - triple backticks (```) to format/enclose console logs 
    - attach long logs as a text file.
-->

#### Summary:

...

#### Steps to reproduce:

- open Inkscape
- ...

#### What happened?

...

#### What should have happened?

...

#### Version Info:

- Inkscape Version: ... 
- Operating System: `...`
- Operating System version: `...`