# Bug Wrangler's Guide

These guidelines detail describe how bug wranglers should address and manage issues. For information on creating a new issue, please see the [bug reporting guidelines](https://inkscape.org/contribute/report-bugs).

If something is missing (or you just need help), ask a question in the [#bug_migration channel](https://chat.inkscape.org/channel/bug_migration).

## <a name="Contents">Table of Contents</a>

### Tasks

1. <a href="#Testing_the_Issue">Testing the Issue</a>
2. <a href="#Addressing_Feature_Requests">Addressing Feature Requests</a>
3. <a href="#Addressing_UX_Issues">Addressing UX Issues</a>
4. <a href="#Labelling_Issues">Labelling Issues</a>
5. <a href="#Moving Issues">Moving Issues</a>
6. <a href="#Closing_Issues">Closing Issues</a>
7. <a href="#Providing_Support">Providing Support</a>
8. <a href="#Migrating_Bugs">Migrating Bugs</a>

### Tips

9. <a href="#Formatting">Formatting</a>
10. <a href="#Quick_Actions">Quick Actions</a>
11. <a href="#Checking_User_Preferences">Checking User Preferences</a>
12. <a href="#Collecting_Backtraces">Collecting Backtraces</a>

# Tasks

## <a name="Testing_the_Issue">Testing the Issue</a>
Before testing, read the issue and **check for similar or duplicate issues**. These often provide further insight into the issue. Link to these issues and post any interesting insights.

If a duplicate issue already exists, the bug should be tracked in a single issue. The other issue should then be [closed](#Closing_Issues). To speed things up, you can search [all Inkscape projects at once](https://gitlab.com/groups/inkscape/-/issues), and search open Launchpad issues [here](https://bugs.launchpad.net/inkscape/+bugs?field.searchtext=&field.status:list=NEW&field.status:list=CONFIRMED&field.status:list=TRIAGED&field.status:list=INPROGRESS&field.status:list=INCOMPLETE_WITH_RESPONSE&field.status:list=INCOMPLETE_WITHOUT_RESPONSE&assignee_option=any&field.assignee=&field.bug_reporter=&field.bug_commenter=&field.subscriber=&field.structural_subscriber=&field.tag=-bug-migration&field.tags_combinator=ANY&field.has_cve.used=&field.omit_dupes.used=&field.omit_dupes=on&field.affects_me.used=&field.has_patch.used=&field.has_branches.used=&field.has_branches=on&field.has_no_branches.used=&field.has_no_branches=on&field.has_blueprints.used=&field.has_blueprints=on&field.has_no_blueprints.used=&field.has_no_blueprints=on&search=Search&orderby=-id&start=0).

After testing, **post your results**, with your Inkscape version, OS and any other relevant information. This allows others to see that the bug is reproducible and may narrow down the problem. Please spend a reasonable amount of time trying to replicate the issue before declaring that you can't replicate the issue.

**Ask for missing or useful information** if you cannot replicate the issue. Label the issue as ~"needs info" if the issue lacks enough information to triage further without the original users input.

**Clean up the issue.** Issues should be clear and concise (so developers can quickly find and read the issue). As a bug wrangler, you should be able to edit the description and title.

The description and title should be edited to clearly describe the problem. This includes summarising lengthy discussion, or simplifying the steps to reproduce, or formatting the issue (see [Gitlab's formatting guide](https://gitlab.com/help/user/markdown)).

Issues should be in English and videos and images should be described in the description. Developers tend to skip over videos as they are often lengthy and unclear.

Extra bugs or suggestions should be moved off to separate issues so they can be searched and tracked separately. The reporter may be willing to help with the cleanup.

**Be polite.** Sometimes you may be in a bad mood or the user may be unreasonable or uncooperative. If you can't provide a polite response, take a break from the issue.

## <a name="Addressing_Feature_Requests">Addressing Feature Requests</a>
**Guide users through the process** of making a feature request. Set proper expectations for users who make feature requests, and show that their voice is heard.

> New features take time to be added into Inkscape, even features that everyone wants. Features require developers who are willing to devote a lot of time and effort. Even then, features won't be introduced into bug-fix releases or fast approaching releases.

> Before a feature is developed, we need to define the scope of the request. Depending on the feature, it helps to include:
> 
> - use cases for the feature
> - a detailed description of the feature
> - screen shots
> - mock-ups
> - prototypes
> 
> Then, we need to find someone who can begin working on the issue. This is similar to how UX suggestions are implemented (see the draft [how to contribute to the Inkscape user experience](https://gitlab.com/inkscape/vectors/content/issues/44)).

> Our motto is *Patch first, ask questions later*

Feature requests can be moved if they are well-defined and there is agreement that the feature will eventually be included in Inkscape. It is not enough that people agree that the feature is nice to have. At the moment, there isn't really a pressing need to move any feature requests.

## <a name="Addressing_UX_Issues">Addressing UX Issues</a>

**UX issues should remain in the Inbox until a UX decision is made**. These issues require a decision to be made on Inkscape behavior. UX issues are moved to the UX sub-project by the UX team when they are being discussed.

Issues should only be moved to inkscape/inkscape or other specific projects when the discussion/research has lead to an agreed upon solution. Like with feature requests, the moving may not be necessary (if a developer is already working on a patch).

In some cases, UX research and studies may be necessary. Keep in mind that we currently do not conduct a lot of UX studies.

In other cases, the main barrier to change is the lack of developers. In these cases, this document describes how a user should push for a change: [how to contribute to the Inkscape user experience](https://gitlab.com/inkscape/vectors/content/issues/44).

## <a name="Labelling_Issues">Labelling Issues</a>

Each project uses its own labelling system. The inbox uses a light labelling system, while other projects have more specific labels. A link to all the labels is present in the left sidebar of each project. This provides a short description of the label, and a link to a list of example issues.

Issues should be labelled with all the labels appropriate for them.  Importance labels should be put on issues in the [Inkscape project](https://gitlab.com/inkscape/inkscape/issues) when they are moved there.

Check with other bug wranglers before adding new labels (you can post in [#bug_migration](https://chat.inkscape.org/channel/bug_migration)).

## <a name="Moving Issues">Moving Issues</a>

Bug Wranglers can move issues to any [project shared with the group](https://gitlab.com/groups/inkscape/bug-wranglers/-/shared).

Read the description of each project (and look at the issues in the tracker) to determine where issues belong.

**Conditions for moving a bug out of Inbox:**

- There are no duplicate issues
- The bug is reproduced by someone besides the original reporter
- The issue has been tidied up (see [Testing the issue](#Testing_the_Issue))
- The issue is labelled appropriately once moved

**Special cases for moving issues:**

- Issues related the Inkscape code-base or building Inkscape should be moved to [Inkscape](https://gitlab.com/inkscape/inkscape/issues), even if it hasn't been replicated.
- Unconfirmed/Unreplicated issues should be moved to Inbox.
- [Feature requests](#Addressing_Feature_Requests) are usually left in Inbox.

After moving the issue, GitLab closes the current issue and opens another one in the appropriate project (related issues are updated appropriately). You may want to lock the closed issue to prevent people accidentally commenting in the wrong place.

## <a name="Closing_Issues">Closing Issues</a>
**Close duplicate issues**. This provides a single point of reference for the bug. The issue that was created first should be kept, unless it was in launchpad or the newer issue is significantly higher quality. Add any new information from the closed duplicate to the original.

You can use [quick actions](#Quick_Actions) to mark the issue as a duplicate.

**Close abandoned issues** to keep the inbox organized. These issues should already have a ~"needs info" label on them.

Abandoned issues:
- Lack enough information to triage further
- Require the original reporter to add missing information (last comment should be requesting this)
- Are untouched for 2 months (no response from reporter, bug wrangler, or anyone else replicating the issue)

Note that the original reporter can reopen the issue if they come back to it.

**Close fixed issues** unless they still occur on a development branch of Inkscape. Leave a comment mentioning the tested Inkscape version. If you can find it, add the version it was fixed, or the relevant merge request and original issue.

## <a name="Providing_Support">Providing Support</a>

Some issues are simple questions about how to use Inkscape, or requests for features that already exist. Help them out. If its too complicated, its best to redirect them to our sparkling new [forum](https://inkscape.org/forums/) or [other communication channels](https://inkscape.org/community/).

## <a name="Migrating_Bugs">Migrating Bugs</a>
Bugs need to be moved from our legacy bug tracker to GitLab. See http://alpha.inkscape.org/bug-migration/

# Tips and Tricks

## <a name="Formatting">Formatting</a>
GitLab uses [markdown](https://gitlab.com/help/user/markdown) to format comments and descriptions, with [syntax highlighting for code blocks](https://gitlab.com/help/user/markdown#colored-code-and-syntax-highlighting).

<details>
<summary>Using Expanders</summary>
You can also create an expander to hide details. Use the preview before posting as the expander may break markdown formatting. To fix, you can use replace the markdown with some simple html.
<pre>
// some code
</pre>
</details>

```html
<details>
<summary>Using Expanders</summary>
You can also create an expander to hide details. Use the preview before posting as the expander may break markdown formatting. To fix, you can use replace the markdown with some simple html.
<pre>
// some code
</pre>
</details>
```

GitLab also allows you to easier [link to merge requests and issues easily]().

## <a name="Quick_Actions">Quick Actions</a>
GitLab supports [quick actions](https://gitlab.com/help/user/project/quick_actions) for labelling and many other actions. This is usually quicker than using the sidebar.

Simply type the quick action when creating (not editing) a comment or description. GitLab should have a pop-up appear that shows an example of the command.

Common quick actions:
- `/close`
- `/duplicate inkscape#124` or `duplicate https://gitlab.com/inkscape/inkscape/issues/124`
- `/label ~bug ~Inkscape`
- `/move inkscape/inkscape`
- `/lock`
- `/title A new title for this issue`
- `/relate inkscape#124` or `/relate https://gitlab.com/inkscape/inkscape/issues/124`

## <a name="Checking_User_Preferences">Checking User Preferences</a>
If you can't replicate the issue, usually its because of differences in the preferences. Asking the user to reset their preferences.xml file is the quickest way to test (make sure they back it up). You probably need to reset yours too!

### Locating preferences.xml
1. Open Inkscape
2. Go to the preferences dialog
3. Go to the System tab
4. Under System Info, User Preference lists its location

### Backing up preferences.xml
In 1.0beta2 and later
1. Open Inkscape
2. Go to the preferences dialog
3. Go to the System tab
4. Under System Info, there is a Reset Preferences button.

In general
1. Close all instances of Inkscape
2. Navigate to the preferences.xml file
3. Rename it to preferences.xml.bak or something else

Dialogs may remain open till restart (even when they are closed or minimised), so it is worth asking the user to restart Inkscape and test without opening any dialogs.

## <a name="Collecting_Backtraces">Collecting Backtraces</a>
This is usually useful for understanding crashes.

### Linux

We use gdb to collect a backtrace from Inkscape. Without compiling, these backtraces will be limited, but should give a better idea of the problem. 

We maintain a guide to [debugging with GDB](https://inkscape.org/develop/debugging/) more generally. If you're interested, play around and research to get a better idea on how to use this tool. However, here are the minimal steps to running `gdb`.

1. Install `gdb`.
1. Run `gdb path/to/inkscape` in the terminal (`gdb inkscape` may be sufficient).
2. Input `set pagination off` and press Enter.
3. Input `run` and press Enter.
4. This should open Inkscape (it will run much slower).
5. Once Inkscape crashes, switch over to the terminal.
6. Input `backtrace` and press Enter (if applicable).
7. Copy the entire output to a text file then exit gdb.

### Mac

You should be able to use `gdb`; however, there should already be crash logs available.

1. Open Inkscape and crash it.
1. Navigate to `~/Library/Logs/DiagnosticReports` in the Finder.
1. The .crash file (should be `inkscape_(date and time) (computer name).crash`) can then be uploaded

### Windows

For Inkscape 1.0beta2 or later, simply run gdb_create_backtrace.bat (it is located in the bin directory of the inkscape folder). When Inkscape crashes, this should create a backtrace and paste it to `%USERPROFILE%\inkscape_backtrace.txt`.